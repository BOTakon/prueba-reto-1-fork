# language: es


  @FeatureName:Capitalesdelmundo

  Característica: Capitales del mundo
    Como Cartografo del mundo
    deseo poder conocer las capitales principales
    para poder tener un conocimiento más amplio


  @ScenarioName:EncontrarCapitales
    Escenario: Encontrar Capitales
      Dado  El Cartografo estaba ubicado en la pagina
      Cuando ingresa el Iso Code de un pais
      Entonces recibe en pantalla la capital de dicho pais
