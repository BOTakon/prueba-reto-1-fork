# language: es
@FeatureName: monedaDelPais
Característica: Moneda del pais
  Como inversionista de negocios internacionales
  Deseo saber la moneda que se usa en un determindado pais
  Para poder calcular la tasa de cambio del valor de mis importaciones y exportaciones

  @ScenarioName: codigoISOYNombreDeMoneda

  Escenario: obtener codigo ISO y nombre de moneda
    Dado que el inversionista paso el codigo ISO del pais Colombia como "CO" al recurso
    Cuando solicite el codigo ISO y el nombre de la moneda de ese pais
    Entonces obtendra como respuesta "COP" y "Pesos"