package co.com.sofka.stepsdefinitions.checkout;

import co.com.sofka.exception.ValidationTextDoNotMatch;
import co.com.sofka.setup.Setup;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;

import static co.com.sofka.question.checkout.Checkout.termsAndConditionsValidation;
import static co.com.sofka.task.checkout.LlenarCheckout.llenarCheckout;
import static co.com.sofka.task.checkout.ReservarViaje.reservarViaje;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class CheckoutStepDefinition extends Setup {

    private static final String ACTOR_NAME = "User";


    @Dado("que el usuario reservo un viaje")
    public void queElUsuarioReservoUnViaje() {
        actorSetupTheBrowser(ACTOR_NAME);
        theActorInTheSpotlight().wasAbleTo(
                reservarViaje()
        );

    }

    @Cuando("intenta pagar sin aceptar los terminos y condiciones")
    public void intentaPagarSinAceptarLosTerminosYCondiciones() {
        theActorInTheSpotlight().attemptsTo(
                llenarCheckout()
        );
    }

    @Entonces("el usuario vera en pantalla una alerta")
    public void elUsuarioVeraEnPantallaUnaAlerta() {
        theActorInTheSpotlight().should(
                seeThat(termsAndConditionsValidation(),equalTo(true)).
                        orComplainWith(ValidationTextDoNotMatch.class)
        );

    }
}
