# language: es
Característica: : funcionalidad para ver opciones
  Como usuario en la web
  Quiero desplegar las opciones
  Para  verlas en su totalidad


  Escenario: : elegir un viaje del contenido oculto
    Dado El usuario estaba logueado en la pagina web
    Cuando El usuario accede al contenido del boton cargar mas para elegir
    Entonces El usuario vera el ultimo destino