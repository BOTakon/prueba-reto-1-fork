# language: es

@FeatureName: Cambiarrangodeprecios

Característica:Cambiar rango de precios
  Como cliente interesado en adquirir un viaje
  necesito poder escoger el rango de precios
  para poder encontrar un viaje acorde a mi presupuesto

  @ScenarioName: Disminuirrangodeprecios

  Escenario: Disminuir rango de precios
    Dado Que el usuario se encontraba en la pagina principal
    Cuando Redujo el rango de precios para los viajes
    Entonces encuentra un viaje ajustado a su presupuesto