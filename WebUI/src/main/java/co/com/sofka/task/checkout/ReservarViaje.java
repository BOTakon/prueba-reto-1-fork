package co.com.sofka.task.checkout;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.com.sofka.userinterface.checkout.Checkout.BOOK;

public class ReservarViaje implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Scroll.to(BOOK),
                Click.on(BOOK)
        );
    }

    public static ReservarViaje reservarViaje(){
        return new ReservarViaje();
    }
}
