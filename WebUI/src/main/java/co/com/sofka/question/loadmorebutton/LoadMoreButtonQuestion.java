package co.com.sofka.question.loadmorebutton;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterface.loadmorebutton.LoadMoreButton.TEMP_MESSAGE;

public class LoadMoreButtonQuestion implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {


        return (TEMP_MESSAGE.resolveFor(actor).containsOnlyText("CUOZHOU TEMPERATURES"));
    }

    public static LoadMoreButtonQuestion loadMoreButtonQuestion(){

        return new LoadMoreButtonQuestion();
    }
}
