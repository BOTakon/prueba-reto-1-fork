package co.com.sofka.task.eliminar;


import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import static co.com.sofka.util.Dictionary.RESOURCE;
import static co.com.sofka.util.Dictionary.URLBASE;

public class EliminarDatos implements Task {

    public static EliminarDatos eliminarDatos(){
        return Tasks.instrumented(EliminarDatos.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        SerenityRest.given()
              .relaxedHTTPSValidation()
                .baseUri(URLBASE).log().all()
                .urlEncodingEnabled(false)
                .contentType(ContentType.JSON)
                .when()
                .delete(RESOURCE)
                .then();
    }
}
