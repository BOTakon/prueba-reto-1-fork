package co.com.sofka.task;

import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Patch;

public class doActualizarDatos implements Task {
    private String resource;
    private String bodyRequest;

    public doActualizarDatos usingTheResource(String resource) {
        this.resource = resource;
        return this;
    }

    public doActualizarDatos andBodyRequest(String bodyRequest) {
        this.bodyRequest = bodyRequest;
        return this;
    }


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Patch.to(resource)
                        .with(
                                requestSpecification ->requestSpecification.relaxedHTTPSValidation().log().all()
                                        .contentType(ContentType.JSON)
                                        .body(bodyRequest)
                        )
        );

    }
    public static doActualizarDatos doPatch(){
        return new doActualizarDatos();
    }
}
