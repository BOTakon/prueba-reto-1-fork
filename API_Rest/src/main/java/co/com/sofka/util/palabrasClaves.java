package co.com.sofka.util;

public enum palabrasClaves {
    COMPLETENAME("[name]"),
    JOB("[job]");

    private final String value;

    palabrasClaves(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
