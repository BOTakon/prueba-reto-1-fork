package co.com.sofka.util;

public abstract class Dictionary {

    public static final String URLBASE = "https://reqres.in/";
    public static final String RESOURCE = "api/users/2";
    public static final String CREATE = "/api/users";
    public static final String RESOURCE_LOGIN = "api/login";
    public static final String SINGLE_RESOURCE = "api/unknown/2";
    public static final String EMPTY_STRING = "";

}
