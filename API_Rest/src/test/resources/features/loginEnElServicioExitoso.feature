# language: es
Característica: Loguearse en el servicio
  yo como cliente registrado
  deseo ingresar al aplicativo
  para validar el funcionamiento de los servicios

  Escenario: Actualizar datos
    Dado el usuario esta en la plataforma y desea loguearse en el servicio
    Cuando el usuario logra loguearse con sus credenciales email "eve.holt@reqres.in" y password "cityslicka"
    Entonces el usuario obtendra un codigo de respuesta exitosa y podra ver su token de acceso