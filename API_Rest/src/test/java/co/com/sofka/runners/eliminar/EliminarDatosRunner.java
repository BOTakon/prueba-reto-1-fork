package co.com.sofka.runners.eliminar;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/eliminar/eliminarDatos.feature"},
        glue = {"co.com.sofka.stepdefinitions.eliminar"})
public class EliminarDatosRunner {
}
